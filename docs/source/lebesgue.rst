lebesgue
========

.. default-role:: math

.. toctree::
   :maxdepth: 2

.. module:: recursivenodes.lebesgue

.. _lebesgue:

The Lebesgue function
---------------------

.. autofunction:: lebesgue

.. autofunction:: lebesguemin

.. autofunction:: lebesgueminhess

.. _lebesgueconstant:

The Lebesgue constant
---------------------

.. autofunction:: lebesguemax

.. autofunction:: lebesguemax_reference
